<?php
require_once('animal.php');

class Frog extends animal
{
    public function jump()
    {
        echo "Jump : Hop Hop";
    }
}

class Ape extends animal
{
    public $legs = 2;
    public function yell()
    {
        echo "Yell : Auooo";
    }
}
