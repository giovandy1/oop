<?php
require_once('animal.php');
require_once('Frog&Ape.php');

echo "<h3>output akhir</h3>";

$sheep = new animal("Shaun");
echo "Name :" . $sheep->name . "<br>";
echo "legs :" . $sheep->legs . "<br>";
echo "cold blooded :" . $sheep->cold_blooded . "<br>";

echo "<br>";

$kodok = new Frog("buduk");
echo "Name :" . $kodok->name . "<br>";
echo "legs :" . $kodok->legs . "<br>";
echo "cold blooded :" . $kodok->cold_blooded . "<br>";
echo $kodok->jump();

echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->name . "<br>";
echo "legs :" . $sungokong->legs . "<br>";
echo "cold blooded :" . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell();
